<?php

require_once('human.php');

class Female extends Human
{

    protected function getGender()
    {
        return 'female';
    }

}


$male = new Female('Ana', 'Marinkovic', '1,69');
echo $male->getAllHumanData();              // prints "Hello, my name is Ana Marinkovic. I'm a female and 1,69m tall."