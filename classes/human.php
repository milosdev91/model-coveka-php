<?php


/**
 * Human Class
 */
abstract class Human
{

    private $name;
    private $surname;
    private $height;

    function __construct($name='Petar', $surname='Petrovic', $height='1,70')
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->height = $height;
    }

    private function getName()
    {
        return $this->name;
    }

    private function getSurname()
    {
        return $this->surname;
    }

    abstract protected function getGender();

    private function getHeight()
    {
        return $this->height;
    }

    /**
     * Get All Human Data
     *
     * @param void
     *
     * @return string containing all data
     */
    public function getAllHumanData()
    {
        return 'Hello, my name is ' . $this->getName() . ' ' . $this->getSurname() . '.
                I\'m a ' . $this->getGender() . ' and ' . $this->getHeight() . 'm tall.';
    }

}