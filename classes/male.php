<?php

require_once('human.php');

class Male extends Human
{

    protected function getGender()
    {
        return 'male';
    }

}


$male = new Male('Jovan', 'Jovanovic', '1,77');
echo $male->getAllHumanData();              // prints "Hello, my name is Jovan Jovanovic. I'm a male and 1,77m tall."